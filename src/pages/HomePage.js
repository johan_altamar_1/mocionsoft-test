import { useContext } from "react";
import { useHistory } from "react-router"
import Swal from "sweetalert2";
import Button from "../components/Button";
import Container from "../components/Container";
import AppContext from "../context/AppContext";

const HomePage = () => {
  const history = useHistory();
  const { setQuestions } = useContext(AppContext)

  const handleBeginClick = () => {
    // QUESTIONS FETCH - CHECK FOR QUESTIONS, IF NOT THROW AN ERROR
    fetch("https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean")
      .then(resp => resp.json())
      .then(data => {
        if (data.results) {
          setQuestions(data.results)

          // REDIRECTS TO FIRST QUESTION
          history.replace("/questions/0")
        } else {
          throw new Error("No Questions available")
        }
      })
      .catch(err => {
        console.error(err)
        Swal.fire("Error", err.toString(), "error")
      })

  }

  return (
    <Container maxWidth="xs">
      <h1 className="text-4xl">Welcome to the Trivia Challenge</h1>
      <h3>You will be presented with 10 True or False questions.</h3>
      <h3>Can you score 100%?</h3>
      <Button onClick={handleBeginClick}>
        Begin
      </Button>
    </Container>
  )
}

export default HomePage
