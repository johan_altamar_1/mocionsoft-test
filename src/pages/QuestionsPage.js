import { useContext, useEffect, useState } from "react"
import { useHistory, useParams } from "react-router";

import Button from "../components/Button";
import ButtonsContainer from "../components/ButtonsContainer";
import Container from "../components/Container";
import QuestionCard from "../components/Question/Card";
import AppContext from "../context/AppContext"
import useCheckQuestions from "../hooks/useCheckQuestions";

const QuestionsPage = () => {
  // CHECK IF QUESTIONS EXISTS; IF NOT REDIRECT TO HOME
  useCheckQuestions();

  const history = useHistory();
  const { questions, setQuestions } = useContext(AppContext);
  const { id } = useParams();

  const [questionInfo, setQuestionInfo] = useState(null)

  // CHECK IF QUESTION INFO EXISTS; IF NOT REDIRECT TO HOME
  useEffect(() => {
    if (questions[id]) {
      setQuestionInfo(questions[id])
    }
  }, [id, questions])

  const handleClick = (response) => event => {
    // Update response in current component
    const newResponse = { ...questionInfo, response }
    setQuestionInfo(newResponse)

    // Update new questions array, with current answer
    let newArr = [...questions];
    newArr[id] = newResponse;
    setQuestions(newArr)

    //Check if there is another questions, if not show results
    const arrLen = questions.length;
    if (id < arrLen - 1) {
      history.replace(`/questions/${Number(id) + 1}`)
    } else {
      history.replace("/results")
    }
  }

  return (
    questionInfo && (
      <Container>
        <h1 className="font-bold">{questionInfo.category}</h1>

        <QuestionCard question={questionInfo.question} />

        <ButtonsContainer>
          <Button type="error" onClick={handleClick("False")}>False</Button>
          <Button type="success" onClick={handleClick("True")}>True</Button>
        </ButtonsContainer>

        <h6>{Number(id) + 1} of {questions.length}</h6>
      </Container>
    )
  )
}

export default QuestionsPage
