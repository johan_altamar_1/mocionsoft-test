import { useContext, useEffect, useState } from "react"
import { useHistory } from "react-router";
import Button from "../components/Button";
import Container from "../components/Container";
import QuestionsResultList from "../components/QuestionsResultList";
import AppContext from "../context/AppContext"
import useCheckQuestions from "../hooks/useCheckQuestions";

const ResultsPage = () => {
  // CHECK IF QUESTIONS EXISTS; IF NOT REDIRECT TO HOME
  useCheckQuestions();

  const history = useHistory();
  
  const { questions, setQuestions } = useContext(AppContext);
  
  const [results, setResults] = useState(0);

  useEffect(() => {
    let result = 0;
    questions.forEach(({ response, correct_answer }) => {
      response = response.toUpperCase();
      correct_answer = correct_answer.toUpperCase();

      if (response === correct_answer) {
        result += 1;
      }
    })
    setResults(result)
  }, [questions])

  const handlePlayAgainClick = () => {
    setQuestions([]);
    history.replace("/")
  }

  return (
    <Container>
      <h3 className="font-bold">You scored </h3>
      <h3 className="mb-10 font-bold">{results} / {questions.length}</h3>

      <QuestionsResultList questions={questions} />

      <Button onClick={handlePlayAgainClick}>Play Again?</Button>
    </Container>
  )
}

export default ResultsPage
