import PropTypes from 'prop-types';

const Button = ({ type = "text", children, onClick }) => {
  const color = type === "success" ? "blue" : (type === "error" ? "red" : "none")

  return (
    <button
      className={`bg-${color}-500 hover:bg-${color}-700 text-white font-bold py-2 px-4 rounded uppercase cursor-pointer`}
      onClick={onClick}
    >
      {children}
    </button>
  )
}

export default Button

Button.propTypes = {
  type: PropTypes.oneOf(["error", "success", "text"]),
  children: PropTypes.string.isRequired,
}