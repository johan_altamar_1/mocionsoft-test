import PropTypes from 'prop-types'

const Container = ({ children, maxWidth = "xl" }) => {
  return (
    <div className={`max-w-${maxWidth} min-h-screen m-auto flex flex-col justify-between cursor-default py-10 px-5 text-3xl`}>
      {children}
    </div>
  )
}

Container.propTypes = {
  children: PropTypes.node.isRequired,
  maxWidth: PropTypes.oneOf(["xs", "sm", "md", "xl",])
}

export default Container
