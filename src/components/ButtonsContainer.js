import PropTypes from 'prop-types'

const ButtonsContainer = ({ children }) => {
  return (
    <div className="flex justify-around flex-grow items-center">
      { children}
    </div>
  )
}

ButtonsContainer.propTypes = {
  children: PropTypes.node.isRequired
}

export default ButtonsContainer
