import PropTypes from 'prop-types'

const QuestionsResultListItem = ({ info }) => {
  const { response, question, correct_answer } = info
  return (
    <div className="flex mb-3">
      <span className="mr-2 font-bold">{response === correct_answer ? "+" : "-"}</span>
      <span dangerouslySetInnerHTML={{ __html: question }}>
      </span>
    </div>
  )
}

QuestionsResultListItem.propTypes = {
  info: PropTypes.object.isRequired
}

export default QuestionsResultListItem
