import PropTypes from 'prop-types'
import QuestionsResultListItem from './ListItem'

const QuestionsResultList = ({ questions }) => {
  return (
    <div className="text-left">
      {
        questions.map((question, idx) => (
          <QuestionsResultListItem key={idx} info={question} />
        ))
      }
    </div>
  )
}

QuestionsResultList.propTypes = {
  questions: PropTypes.array.isRequired
}

export default QuestionsResultList
