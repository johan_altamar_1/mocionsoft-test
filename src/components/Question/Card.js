const Card = ({ question }) => {
  return (
    <div className="border-2 border-dashed border-white p-6 flex-grow mt-16 flex items-center">
      <p dangerouslySetInnerHTML={{ __html: question }} />
    </div>
  )
}

export default Card
