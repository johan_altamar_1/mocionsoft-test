import { useContext, useEffect } from "react"
import { useHistory } from "react-router"
import Swal from "sweetalert2"
import AppContext from "../context/AppContext";

const useCheckQuestions = () => {
  const history = useHistory();
  const { questions } = useContext(AppContext);
  
  // CHECK IF QUESTIONS EXISTS; IF NOT REDIRECT TO HOME
  useEffect(() => {
    if (questions.length === 0) {
      Swal.fire("IMPORTANT", "You will be redirected to home, no questions available", "warning").then(() => {
        history.replace("/")
      })
    }
  }, [questions.length, history])
}

export default useCheckQuestions
