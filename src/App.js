import { useState } from 'react';
import './App.css';
import AppContext from './context/AppContext';
import AppRouter from './routers/AppRouter';

function App() {
  const [questions, setQuestions] = useState([]);

  return (
    <AppContext.Provider value={{ questions, setQuestions }}>
      <AppRouter />
    </AppContext.Provider>
  );
}

export default App;
