import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import HomePage from '../pages/HomePage';
import QuestionsPage from '../pages/QuestionsPage';
import ResultsPage from '../pages/ResultsPage';

const AppRouter = () => {
  return (
    <Router>
      <div className="min-h-screen bg-gray-600 text-white text-center">
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/questions/:id" component={QuestionsPage} />
          <Route exact path="/results" component={ResultsPage} />
        </Switch>
      </div>
    </Router>
  )
}

export default AppRouter
